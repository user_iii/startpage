const MONTH_NAMES = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]

const SEARCHBAR = Object.freeze(document.querySelector("#searchbar-input"))
const TIME = Object.freeze(document.querySelector("#time"))
const DATE = Object.freeze(document.querySelector("#date"))

const updateTime = () => {
	let dt = new Date()
	let h = dt.getHours()
	let m = dt.getMinutes()
	let suffix = "AM"

	if (h > 12) {
	    h -= 12
	    suffix = "PM"
	}

	TIME.innerHTML = h + ":" + (m < 10 ? "0" + m : m) + " " + suffix
}

const updateDate = () => {
	let dt = new Date()
	let d = dt.getDate()
	let m = dt.getMonth()
	let y = dt.getFullYear()
	DATE.innerHTML = y + " " + MONTH_NAMES[m] + " " + d
}

if (TIME) {
    updateTime()
    setInterval(() => updateTime(), 1000)
}

if (DATE) {
    updateDate()
    setInterval(() => updateDate(), 5000)
}

if (SEARCHBAR) {
    SEARCHBAR.addEventListener("keyup", e => {
        const term = e.target.value;

        if (e.code == "Enter") {
            window.location.href = "https://duckduckgo.com/?q=" + term;
        }
    })
}

