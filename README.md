startpage
=========
A nice browser homepage.

Support
-------
Browser must support:
- HTML5
- CSS3
- JavaScript (ECMAScript 2016)
